# Contributor: Coco Liliace <coco@liliace.dev>
# Maintainer: Coco Liliace <coco@liliace.dev>
_pyname=irc
pkgname=py3-$_pyname
pkgver=20.4.0
pkgrel=0
pkgdesc="Full-featured Python IRC library"
url="https://github.com/jaraco/irc"
arch="noarch"
license="MIT"
depends="
	py3-jaraco.collections
	py3-jaraco.text
	py3-jaraco.logging
	py3-jaraco.functools
	py3-jaraco.stream
	py3-more-itertools
	py3-tempora
	"
makedepends="py3-gpep517 py3-setuptools_scm py3-wheel"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/jaraco/irc/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/$_pyname-$pkgver"

_distname=".dist"

build() {
	export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver
	gpep517 build-wheel --output-fd 3 --wheel-dir "$_distname" 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer "$_distname"/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	gpep517 install-wheel --destdir "$pkgdir" "$_distname"/*.whl
}

sha512sums="
40987033fc61be543957b43f768b414842510d8b545a91f0b6da5c9eda25cb88b28f93dd0eab0da4cc67be93060fc18e8995ac2785883331d08b639549e94b65  py3-irc-20.4.0.tar.gz
"
