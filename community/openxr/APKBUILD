# Contributor: Leon Marz <main@lmarz.org>
# Maintainer: Leon Marz <main@lmarz.org>
pkgname=openxr
pkgver=1.1.37
pkgrel=0
pkgdesc="OpenXR loader library"
url="https://khronos.org/openxr"
arch="all"
license="Apache-2.0"
makedepends="cmake mesa-dev vulkan-loader-dev jsoncpp-dev samurai wayland-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/KhronosGroup/OpenXR-SDK/archive/release-$pkgver.tar.gz"
builddir="$srcdir/OpenXR-SDK-release-$pkgver"
options="!check" # no check available

build() {
	case "$CARCH" in
		armhf)
			export CXXFLAGS="$CXXFLAGS -D_M_ARM" ;;
	esac

	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_BUILD_TYPE=Release
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
11f020ca83bf34a60f1ecc9837be58e2c59be0e380f6f59f28049247e95f5e9fcf08aa8308e2e82dfab75cb4ca100a5550002f3c281822d97589b3c7d0b1d870  openxr-1.1.37.tar.gz
"
