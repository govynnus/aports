# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: Antoine Martin (ayakael) <dev@ayakael.net>
pkgname=py3-boto3
pkgver=1.34.117
pkgrel=0
pkgdesc="AWS SDK for Python (Boto3)"
url="https://aws.amazon.com/sdk-for-python/"
license="Apache-2.0"
arch="noarch"
depends="
	py3-botocore
	py3-jmespath
	py3-s3transfer
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/b/boto3/boto3-$pkgver.tar.gz"
builddir="$srcdir"/boto3-$pkgver
options="!check" # take way too long

replaces="py-boto3" # Backwards compatibility
provides="py-boto3=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
fb7b78a9a284daf8e2f503972e0f55bbf9216116aada18b07c693ec844c23d5cb0ded4f123680dcd94480f15d13c0cf215f5f6274c6a8531c68f9bb95f3163c2  boto3-1.34.117.tar.gz
"
