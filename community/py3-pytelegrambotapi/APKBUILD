# Contributor: Antoine Martin (ayakael) <dev@ayakael.net>
# Maintainer: Antoine Martin (ayakael) <dev@ayakael.net>
pkgname=py3-pytelegrambotapi
pkgver=4.18.1
# sometimes upstream forgets to tag pypi releases
_gittag=e2aa7a7132fc47da541c97c5fa82811eec54c8b4
pkgrel=0
arch="noarch"
pkgdesc="A simple, but extensible Python implementation for the Telegram Bot API."
url="https://pypi.org/project/pyTelegramBotAPI/"
license="GPL-2.0-only"
depends="py3-requests"
makedepends="
	py3-setuptools
	py3-gpep517
	py3-wheel
	py3-hatchling
	"
checkdepends="
	py3-aiohttp
	py3-pytest
	"
source="$pkgname-$_gittag.tar.gz::https://github.com/eternnoir/pyTelegramBotAPI/archive/$_gittag.tar.gz"
builddir="$srcdir"/pyTelegramBotAPI-$_gittag
subpackages="$pkgname-pyc"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer "$builddir"/.dist/*.whl
	.testenv/bin/python3 -m pytest -v
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
0df3182ccc79726b8fa58945b943546be7fbed072e36325b098308eecbbfb268e24d895ae62cef2f2b36616ddb79b9248892422fc429d1cc8bdff0046ca0a0ac  py3-pytelegrambotapi-e2aa7a7132fc47da541c97c5fa82811eec54c8b4.tar.gz
"
