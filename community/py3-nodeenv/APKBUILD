# Contributor: Duncan Bellamy <dunk@denkimushi.com>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=py3-nodeenv
_pkgname=nodeenv
pkgver=1.9.0
pkgrel=0
pkgdesc="Node.js virtual environment builder"
url="https://ekalinin.github.io/nodeenv/"
arch="all"
license="BSD-3-Clause"
depends="bash py3-setuptools"
makedepends="python3-dev py3-gpep517 py3-wheel"
checkdepends="nodejs py3-pytest py3-mock py3-pytest-cov"
subpackages="$pkgname-pyc"
source="$_pkgname-$pkgver.tar.gz::https://github.com/ekalinin/nodeenv/archive/$pkgver.tar.gz
	11-alpine-prebuilt.patch
	"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
d24612372f7fb6d909ce164f6b980086581b53ee8234c6ec14003a79418d7798481275e87589ade47c0502a4b396fa7b07fcfbb99174f1331f65dd6e7a1e0fd7  nodeenv-1.9.0.tar.gz
e92b8f702965faeae10263f991d942234fad1b8687e1c1d151860a3a5e5f90b802de4869a3f2995ee915a622167be8fc7df6532f3d106a33538a61cd20b52802  11-alpine-prebuilt.patch
"
